package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void addTask(String taskName, String projectId, String userId) {
        if (taskName == null || taskName.isEmpty()) return;
        Task task = new Task();
        task.setName(taskName);
        task.setProjectId(projectId);
        task.setUserId(userId);
        this.taskRepository.persist(task);
    }

    public List<Task> showTasks() {
        List<Task> list = this.taskRepository.findAll();
        return list;
    }

    public List<Task> showTasks(String userId) {
        List<Task> filteredList = this.taskRepository.findAll(userId);
        return filteredList;
    }

    public void updateTask(String taskId, String name) {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) return;
        Task oldTask = this.taskRepository.findOne(taskId);
        String projectId = oldTask.getProjectId();
        Task task = new Task();
        task.setName(name);
        task.setId(taskId);
        task.setProjectId(projectId);
        this.taskRepository.merge(task);
    }

    public void deleteTask(String taskId) {
        if (taskId == null || taskId.isEmpty() || this.taskRepository.findOne(taskId) == null) return;
        this.taskRepository.remove(taskId);
    }

    public void clearTasks(String projectId) {
        List<Task> list = this.taskRepository.findAll();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) {
                this.taskRepository.remove(task.getId());
            }
        }
    }
}
