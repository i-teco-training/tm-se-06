package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.UserRepository;

import java.util.List;

public class UserService {
    private UserRepository userRepository;
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean isAuthorized() {
        return this.currentUser != null;
    }

    public void addUser(User user) {
        if (user == null) return;
        this.userRepository.persist(user);
    }

    public User findUser(String login, String passwordHashcode) {
        return this.userRepository.findOne(login,passwordHashcode);
    }

    public List<User> showUsers() {
        List<User> list = this.userRepository.findAll();
        return list;
    }

    public void updateUser(User user) {
        if (user == null) return;
        this.userRepository.merge(user);
    }

    public void deleteUser(String login, String passwordHashcode) {
        User userForExistenceChecking = this.userRepository.findOne(login,passwordHashcode);
        if (userForExistenceChecking == null) return;
        this.userRepository.remove(userForExistenceChecking.getId());
    }

    public void clearUsers() {
        this.userRepository.removeAll();
    }
}
