package ru.alekseev.tm.bootstrap;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.project.*;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.command.system.ExitCommand;
import ru.alekseev.tm.command.system.HelpCommand;
import ru.alekseev.tm.command.task.*;
import ru.alekseev.tm.command.user.*;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;
import ru.alekseev.tm.service.UserService;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class Bootstrap implements ServiceLocator {
    private ProjectRepository projectRepository = new ProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskRepository taskRepository = new TaskRepository();
    private TaskService taskService = new TaskService(taskRepository);
    private UserRepository userRepository = new UserRepository();
    private UserService userService = new UserService(userRepository);

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    public void registry(AbstractCommand command) {
        String commandName = command.getName();
        commands.put(commandName, command);
    }

    public void init() throws Exception { //надо поймать Exception(?)
        registry(new HelpCommand(this));
        registry(new UserSignupCommand(this));
        registry(new UserLoginCommand(this));
        registry(new UserLogoutCommand(this));
        registry(new UserLoadCurrentProfileCommand(this));
        registry(new UserEditCurrentProfileCommand(this));
        registry(new UserPasswordUpdateCommand(this));
        registry(new ProjectAddCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectDeleteCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectUpdateCommand(this));
        registry(new TaskAddCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskDeleteCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskUpdateCommand(this));
        registry(new ExitCommand(this));

        //создать 2 пользователя(пароль "www")
        User userAdminRole = new User();
        userAdminRole.setRoleType(RoleType.admin);
        userAdminRole.setLogin("user1");
        userAdminRole.setPasswordHashcode(HashUtil.getMd5("www"));
        this.userService.addUser(userAdminRole);

        User userUserRole = new User();
        userUserRole.setRoleType(RoleType.user);
        userUserRole.setLogin("user2");
        userUserRole.setPasswordHashcode("4eae35f1b35977a00ebd8086c259d4c9");
        this.userService.addUser(userUserRole);

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            String commandName = reader.readLine();// оперировать мапой ок? или надо оперировать листом?
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && !this.userService.isAuthorized()) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            commands.get(commandName).execute();
            System.out.println();
        }
    }
}