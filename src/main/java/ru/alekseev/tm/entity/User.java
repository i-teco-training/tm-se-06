package ru.alekseev.tm.entity;

import java.util.UUID;

public class User {
    private String id = UUID.randomUUID().toString();
    private String login;
    private String passwordHashcode;
    private RoleType roleType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHashcode() {
        return passwordHashcode;
    }

    public void setPasswordHashcode(String passwordHashcode) {
        this.passwordHashcode = passwordHashcode;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
