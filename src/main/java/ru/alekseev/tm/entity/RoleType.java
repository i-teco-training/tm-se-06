package ru.alekseev.tm.entity;

public enum RoleType {
    user("пользователь"),
    admin("администратор");

    private String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }


    @Override
    public String toString() {
        return this.displayName;
    }
}
