package ru.alekseev.tm.repository;

import ru.alekseev.tm.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {
    private Map<String, User> users = new HashMap<>();

    public List<User> findAll() {
        List<User> list = new ArrayList<>(users.values());
        return list;
    }

    public User findOne(String login, String passwordHashcode) {
        List<User> list = new ArrayList<>(users.values());
        for (User user : list) {
            if (login.equals(user.getLogin()) && passwordHashcode.equals(user.getPasswordHashcode()))
                return user;
        }
        return null;
    }

    public void persist(User user) {
        if (!users.containsKey(user.getId())) {
            users.put(user.getId(), user);
        }
    }

    public void merge(User user) {
        users.put(user.getId(), user);
    }

    public void remove(String userId) {
        users.remove(userId);
    }

    public void removeAll() {
        users.clear();
    }
}
