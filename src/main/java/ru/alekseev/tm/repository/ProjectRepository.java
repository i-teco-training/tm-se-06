package ru.alekseev.tm.repository;

import ru.alekseev.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {
    private Map<String, Project> projects = new HashMap<>();

    public List<Project> findAll() {
        List<Project> list = new ArrayList<>(projects.values());
        return list;
    }

    public List<Project> findAll(String userId) {
        List<Project> list = new ArrayList<>(projects.values());
        List<Project> filteredList = new ArrayList<>();
        for (Project project : list) {
            if (project.getUserId().equals(userId))
                filteredList.add(project);
        }
        return filteredList;
    }

    public Project findOne(String projectId) {
        return projects.get(projectId);
    }

    public void persist(Project project) {
        if (!projects.containsKey(project.getId())) {
            projects.put(project.getId(), project);
        }
    }

    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(String projectId) {
        projects.remove(projectId);
    }

    public void removeAll() {
        projects.clear();
    }
}
