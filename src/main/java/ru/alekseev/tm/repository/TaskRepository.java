package ru.alekseev.tm.repository;

import ru.alekseev.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();

    public List<Task> findAll() {
        List<Task> list = new ArrayList<>(tasks.values());
        return list;
    }

    public List<Task> findAll(String userId) {
        List<Task> list = new ArrayList<>(tasks.values());
        List<Task> filteredList = new ArrayList<>();
        for (Task task : list) {
            if (task.getUserId().equals(userId))
                filteredList.add(task);
        }
        return filteredList;
    }

    public Task findOne(String taskId) {
        return tasks.get(taskId);
    }

    public void persist(Task task) {
        if (!tasks.containsKey(task.getId())) {
            tasks.put(task.getId(), task);
        }
    }

    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(String taskId) {
        tasks.remove(taskId);
    }
}
