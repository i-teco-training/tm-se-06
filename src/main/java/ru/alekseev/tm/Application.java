package ru.alekseev.tm;

import ru.alekseev.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
