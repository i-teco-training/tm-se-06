package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {
    public UserLogoutCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGGING OUT]");
        this.serviceLocator.getUserService().setCurrentUser(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
