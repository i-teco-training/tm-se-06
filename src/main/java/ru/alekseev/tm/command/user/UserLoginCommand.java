package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserLoginCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UserLoginCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "Login into Project Manager";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGGING IN]");
        System.out.println("ENTER LOGIN");
        String login = reader.readLine();
        System.out.println("ENTER PASSWORD");
        String password = reader.readLine();
        String passwordHashcode = HashUtil.getMd5(password);
        User requiredUser = this.serviceLocator.getUserService().findUser(login, passwordHashcode);
        this.serviceLocator.getUserService().setCurrentUser(requiredUser);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
