package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserEditCurrentProfileCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UserEditCurrentProfileCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "edit-profile";
    }

    @Override
    public String getDescription() {
        return "Edit your Project Manager profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDITING YOUR PROFILE]");
        User currentUser = this.serviceLocator.getUserService().getCurrentUser();
        System.out.println("If you want to update account type enter \"1\" otherwise enter any letter");
        String accountTypeUpdateChoice = reader.readLine();
        if ("1".equals(accountTypeUpdateChoice)) {
            System.out.println("Enter \"1\" to chose \"admin\" account type otherwise enter any letter");
            String newAccountType = reader.readLine();
            if ("1".equals(newAccountType)) {
                currentUser.setRoleType(RoleType.admin);
            } else currentUser.setRoleType(RoleType.user);
        }
        this.serviceLocator.getUserService().updateUser(currentUser);
        System.out.println("[YOUR PROFILE IS UPDATED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
