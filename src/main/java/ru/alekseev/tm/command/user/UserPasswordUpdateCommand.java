package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserPasswordUpdateCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UserPasswordUpdateCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "password-update";
    }

    @Override
    public String getDescription() {
        return "Change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD UPDATING]");
        System.out.println("ENTER LOGIN");
        String login = reader.readLine();
        System.out.println("ENTER PASSWORD");
        String password = reader.readLine();
        String passwordHashcode = HashUtil.getMd5(password);
        User requiredUser = this.serviceLocator.getUserService().findUser(login, passwordHashcode);
        System.out.println("ENTER NEW PASSWORD");
        String newPassword = reader.readLine();
        String newPasswordHashcode = HashUtil.getMd5(newPassword);
        requiredUser.setPasswordHashcode(newPasswordHashcode);
        this.serviceLocator.getUserService().updateUser(requiredUser);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
