package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserSignupCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UserSignupCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "signup";
    }

    @Override
    public String getDescription() {
        return "Create new user account";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADDING OF NEW USER ACCOUNT]");
        System.out.println("ENTER LOGIN");
        String login = reader.readLine();
        System.out.println("ENTER PASSWORD");
        String password = reader.readLine();
        String passwordHashcode = HashUtil.getMd5(password);
        User newUser = new User();
        newUser.setLogin(login);
        newUser.setPasswordHashcode(passwordHashcode);
        this.serviceLocator.getUserService().addUser(newUser);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
