package ru.alekseev.tm.command.project;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectUpdateCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectUpdateCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "update-project";
    }

    @Override
    public String getDescription() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        this.serviceLocator.getProjectService().updateProject(projectId, newName);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
