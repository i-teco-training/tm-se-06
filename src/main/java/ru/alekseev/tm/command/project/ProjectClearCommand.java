package ru.alekseev.tm.command.project;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;

import java.util.List;

public class ProjectClearCommand extends AbstractCommand {
    public ProjectClearCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "clear-projects";
    }

    @Override
    public String getDescription() {
        return "Delete all projects";
    }

    @Override
    public void execute() throws Exception {
        List<Project> list = this.serviceLocator.getProjectService().showProjects();
        for (Project project : list) {
            this.serviceLocator.getTaskService().clearTasks(project.getId());
        }
        this.serviceLocator.getProjectService().clearProjects();
        System.out.println("[OK. ALL PROJECTS AND THEIR TASKS DELETED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
