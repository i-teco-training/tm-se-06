package ru.alekseev.tm.command.project;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    public ProjectListCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "show-projects";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LIST OF ALL PROJECTS]");
        User currentUser = this.serviceLocator.getUserService().getCurrentUser();

        if (currentUser.getRoleType() == RoleType.admin) {
            List<Project> list = this.serviceLocator.getProjectService().showProjects();
            if (list.size() == 0) {
                System.out.println("LIST OF PROJECTS IS EMPTY");
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.print(i + 1);
                System.out.println(") name:" + list.get(i).getName() + ", projectId: " + list.get(i).getId() + ", userId: " + list.get(i).getUserId());
            }
        } else {
            List<Project> list = this.serviceLocator.getProjectService().showProjects(currentUser.getId());
            if (list.size() == 0) {
                System.out.println("LIST OF PROJECTS IS EMPTY");
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.print(i + 1);
                System.out.println(") name:" + list.get(i).getName() + ", projectId: " + list.get(i).getId());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
