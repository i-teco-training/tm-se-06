package ru.alekseev.tm.command.task;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskDeleteCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskDeleteCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "delete-task";
    }

    @Override
    public String getDescription() {
        return "Delete task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETING OF TASK]");
        System.out.println("ENTER TASK ID");
        String taskId = reader.readLine();
        this.serviceLocator.getTaskService().deleteTask(taskId);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
