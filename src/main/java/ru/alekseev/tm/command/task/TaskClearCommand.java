package ru.alekseev.tm.command.task;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskClearCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskClearCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "clear-tasks";
    }

    @Override
    public String getDescription() {
        return "Delete all tasks attached to project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEARING OF TASKS BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        this.serviceLocator.getTaskService().clearTasks(projectId);
        System.out.println("[ALL TASKS ATTACHED TO PROJECT (projectId = " + projectId + ") DELETED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
