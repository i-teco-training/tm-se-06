package ru.alekseev.tm.command.task;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskAddCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskAddCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "add-task";
    }

    @Override
    public String getDescription() {
        return "Add new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADDING OF NEW TASK]");
        System.out.println("TYPE \"1\" TO ATTACH NEW TASK TO EXISTING PROJECT (otherwise press \"ENTER\" key)");
        String userChoice = reader.readLine();
        String projectId = null;
        if ("1".equals(userChoice)) {
            System.out.println("ENTER PROJECT ID");
            projectId = reader.readLine();
        }
        System.out.println("ENTER TASK NAME");
        String taskName = reader.readLine();
        String currentUserId = this.serviceLocator.getUserService().getCurrentUser().getId();
        this.serviceLocator.getTaskService().addTask(taskName, projectId, currentUserId);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
