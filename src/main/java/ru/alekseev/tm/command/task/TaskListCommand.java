package ru.alekseev.tm.command.task;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.List;

public class TaskListCommand extends AbstractCommand {
    public TaskListCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "show-tasks";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LIST OF ALL TASKS]");
        User currentUser = this.serviceLocator.getUserService().getCurrentUser();

        if (currentUser.getRoleType() == RoleType.admin) {
            List<Task> list = this.serviceLocator.getTaskService().showTasks();
            if (list.size() == 0) {
                System.out.println("LIST OF TASKS IS EMPTY");
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.print(i + 1);
                System.out.println(") name:" + list.get(i).getName() + ", taskId: " + list.get(i).getId() + ", userId: " + list.get(i).getUserId());
            }
        } else {
            List<Task> list = this.serviceLocator.getTaskService().showTasks(currentUser.getId());
            if (list.size() == 0) {
                System.out.println("LIST OF TASKS IS EMPTY");
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.print(i + 1);
                System.out.println(") name:" + list.get(i).getName() + ", taskId: " + list.get(i).getId());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
