package ru.alekseev.tm.api;

import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;
import ru.alekseev.tm.service.UserService;

import java.util.List;

public interface ServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    List<AbstractCommand> getCommands();
}
